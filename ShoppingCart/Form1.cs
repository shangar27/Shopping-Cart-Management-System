﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingCart
{
    public partial class Form1 : Form
    {
        // Declare Array of Item Cost Elements 
        double[] itemcost = new double[7];
        double iTax, iSubTotal, iTotal;

        //Initialize components
        public Form1()
        {
            InitializeComponent();
        }

        //Exit button functionality
        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult iExit;

            iExit = MessageBox.Show("Confirm if you want to exit", "POS", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (iExit == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        //*****************************Reset**********************************
        private void ResetTextBox()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Text = "0";
                   else
                        func(control.Controls);
            };
            func(Controls);
        }

        //********************************************************************

        //*****************************EnableTextBox**************************
        private void EnableTextBox()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox)
                        (control as TextBox).Enabled = false;
                    else
                        func(control.Controls);
            };
            func(Controls);
        }

        //********************************************************************

        //*****************************EnableCheckBox**************************
        private void EnableCheckBox()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is CheckBox)
                        (control as CheckBox).Enabled = false;
                    else
                        func(control.Controls);
            };
            func(Controls);
        }

        //********************************************************************

        //*****************************CheckBox=Checked**************************
        private void CheckBoxChecked()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is CheckBox)
                        (control as CheckBox).Checked = false;
                    else
                        func(control.Controls);
            };
            func(Controls);
        }

        //********************************************************************

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetTextBox();
            EnableTextBox();
            EnableCheckBox();
            CheckBoxChecked();

            txtSubTotal.Text = "";
            txtTax.Text = "";
            txtTotal.Text = "";
            multiTxtReceipt.Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnableTextBox();
            EnableCheckBox();
        }

        private void btnApples_Click(object sender, EventArgs e)
        {
            chkApples.Enabled = true;
        }

        private void btnOranges_Click(object sender, EventArgs e)
        {
            chkOranges.Enabled = true;
        }

        private void changeColour(object sender, EventArgs e)
        {
            Button bColour = (Button)sender;
            bColour.BackColor = Color.LightSteelBlue;
        }

        private void colourChange(object sender, EventArgs e)
        {
            Button bColour = (Button)sender;
            bColour.BackColor = Color.FromKnownColor(KnownColor.Control);
        }

        private void chkApples_CheckedChanged(object sender, EventArgs e)
        {
            if (chkApples .Checked == true)
            {
                txtTotApples.Enabled = true;
                txtTax.Enabled = true;
                txtSubTotal.Enabled = true;
                txtTotal.Enabled = true;
                multiTxtReceipt.Enabled = true;
                txtTotApples.Text = "";
                txtTotApples.Focus();
            }

            if (chkApples.Checked == false)
            {
                txtTotApples.Enabled = false;
                txtTax.Enabled = false;
                txtSubTotal.Enabled = false;
                txtTotal.Enabled = false;
                multiTxtReceipt.Enabled = false;
                txtTotApples.Text = "";                
            }
        }

        private void chkOranges_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOranges.Checked == true)
            {
                txtTotOranges.Enabled = true;
                txtTax.Enabled = true;
                txtSubTotal.Enabled = true;
                txtTotal.Enabled = true;
                multiTxtReceipt.Enabled = true;
                txtTotOranges.Text = "";
                txtTotOranges.Focus();
            }

            if (chkOranges.Checked == false)
            {
                txtTotOranges.Enabled = false;
                txtTax.Enabled = false;
                txtSubTotal.Enabled = false;
                txtTotal.Enabled = false;
                multiTxtReceipt.Enabled = false;
                txtTotOranges.Text = "";
            }
        }

        // Key in validations
        private void NumbersOnly(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;

            if (!char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        //Mouse hover validation for empty text box values
        private void over(object sender, EventArgs e)
        {
            if (txtTotApples.Text == "")
            {
                txtTotApples.Text = "0";
                txtTotApples.Enabled = false;
                chkApples.Checked = false;
                chkApples.Enabled = false;
            }

            if (txtTotOranges.Text == "")
            {
                txtTotOranges.Text = "0";
                txtTotOranges.Enabled = false;
                chkOranges.Checked = false;
                chkOranges.Enabled = false;
            }
        }

        //Final total value and customer receipt
        private void btnTotal_Click(object sender, EventArgs e)
        {
            double Tax_Rate = 0.15;

            itemcost[0] = 0.60;
            int numberOfApples = Convert.ToInt16(txtTotApples.Text);

            itemcost[1] = 0.25;
            int numberOfOranges = Convert.ToInt16(txtTotOranges.Text);

            //Apply Simple Offers on Items
            if (numberOfApples > 1)
            {
                itemcost[0] = (numberOfApples / 2) * itemcost[0] + (numberOfApples % 2) * itemcost[0];   //Apples - Buy 1 Get 1 Free
            }
            else
            {
                itemcost[0] = Convert.ToDouble(txtTotApples.Text) * 0.60;
            }

            if (numberOfOranges >= 3)
            {
                itemcost[1] = (numberOfOranges / 3) * 2 * itemcost[1] + (numberOfOranges % 3) * itemcost[1];  //Oranges - 3 For 2
            }
            else
            {
                itemcost[1] = Convert.ToDouble(txtTotOranges.Text) * 0.25;
            }     

            itemcost[2] = itemcost[0] + itemcost[1];

            iSubTotal = itemcost[2];

            txtSubTotal.Text = Convert.ToString(iSubTotal);
            iTax = (iSubTotal * Tax_Rate) / 100;
            txtTax.Text = Convert.ToString(iTax);
            iTotal = (iSubTotal + iTax);
            txtTotal.Text = Convert.ToString(iTotal);

            txtTax.Text = String.Format("{0:0.00}", iTax);
            txtSubTotal.Text = String.Format("{0:0.00}", iSubTotal);
            txtTotal.Text = String.Format("{0:0.00}", iTotal);            

            txtTax.Text = String.Format("{0:C}", iTax);
            txtSubTotal.Text = String.Format("{0:C}", iSubTotal);
            txtTotal.Text = String.Format("{0:C}", iTotal);

            multiTxtReceipt.AppendText("\t\t" + " iStore" + Environment.NewLine);
            multiTxtReceipt.AppendText("\t\t" + " 92E Brighton Road" + Environment.NewLine);
            multiTxtReceipt.AppendText("\t\t" + " Worthing" + Environment.NewLine);
            multiTxtReceipt.AppendText("\t\t" + " West Sussex" + Environment.NewLine);
            multiTxtReceipt.AppendText("=========================================" + Environment.NewLine);
            multiTxtReceipt.AppendText("Tax " + "\t\t" + txtTax.Text + Environment.NewLine);
            multiTxtReceipt.AppendText("SubTotal " + "\t\t" + txtSubTotal.Text + Environment.NewLine);
            multiTxtReceipt.AppendText("Total " + "\t\t" + txtTotal.Text + Environment.NewLine);
            multiTxtReceipt.AppendText("=========================================" + Environment.NewLine);
            multiTxtReceipt.AppendText("\t" + "Thank you for shopping at iStore");
        }
    }
}
