﻿namespace ShoppingCart
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblShoppingCart = new System.Windows.Forms.Label();
            this.lblCostOfItem = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.btnApples = new System.Windows.Forms.Button();
            this.btnOranges = new System.Windows.Forms.Button();
            this.chkApples = new System.Windows.Forms.CheckBox();
            this.txtTotApples = new System.Windows.Forms.TextBox();
            this.chkOranges = new System.Windows.Forms.CheckBox();
            this.txtTotOranges = new System.Windows.Forms.TextBox();
            this.lblTax = new System.Windows.Forms.Label();
            this.lblSubTotal = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtTax = new System.Windows.Forms.TextBox();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.btnTotal = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.multiTxtReceipt = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnOranges);
            this.groupBox1.Controls.Add(this.btnApples);
            this.groupBox1.Controls.Add(this.lblCategory);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 571);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.MouseHover += new System.EventHandler(this.over);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblCostOfItem);
            this.groupBox2.Controls.Add(this.lblShoppingCart);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(252, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1074, 571);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.MouseHover += new System.EventHandler(this.over);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtTotOranges);
            this.groupBox3.Controls.Add(this.chkOranges);
            this.groupBox3.Controls.Add(this.txtTotApples);
            this.groupBox3.Controls.Add(this.chkApples);
            this.groupBox3.Location = new System.Drawing.Point(18, 109);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(591, 444);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.MouseHover += new System.EventHandler(this.over);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnExit);
            this.groupBox4.Controls.Add(this.btnReset);
            this.groupBox4.Controls.Add(this.btnTotal);
            this.groupBox4.Controls.Add(this.multiTxtReceipt);
            this.groupBox4.Controls.Add(this.txtTotal);
            this.groupBox4.Controls.Add(this.txtSubTotal);
            this.groupBox4.Controls.Add(this.txtTax);
            this.groupBox4.Controls.Add(this.lblTotal);
            this.groupBox4.Controls.Add(this.lblSubTotal);
            this.groupBox4.Controls.Add(this.lblTax);
            this.groupBox4.Location = new System.Drawing.Point(615, 109);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(435, 444);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.MouseHover += new System.EventHandler(this.over);
            // 
            // lblShoppingCart
            // 
            this.lblShoppingCart.AutoSize = true;
            this.lblShoppingCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShoppingCart.Location = new System.Drawing.Point(3, 42);
            this.lblShoppingCart.Name = "lblShoppingCart";
            this.lblShoppingCart.Size = new System.Drawing.Size(502, 32);
            this.lblShoppingCart.TabIndex = 0;
            this.lblShoppingCart.Text = "Shopping Cart Management System";
            // 
            // lblCostOfItem
            // 
            this.lblCostOfItem.AutoSize = true;
            this.lblCostOfItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCostOfItem.Location = new System.Drawing.Point(622, 42);
            this.lblCostOfItem.Name = "lblCostOfItem";
            this.lblCostOfItem.Size = new System.Drawing.Size(408, 32);
            this.lblCostOfItem.TabIndex = 0;
            this.lblCostOfItem.Text = "Cost of Items and Receipt (£)";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(13, 42);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(163, 32);
            this.lblCategory.TabIndex = 0;
            this.lblCategory.Text = "Categories";
            // 
            // btnApples
            // 
            this.btnApples.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApples.Location = new System.Drawing.Point(6, 99);
            this.btnApples.Name = "btnApples";
            this.btnApples.Size = new System.Drawing.Size(88, 29);
            this.btnApples.TabIndex = 1;
            this.btnApples.Text = "Apples";
            this.btnApples.UseVisualStyleBackColor = true;
            this.btnApples.Click += new System.EventHandler(this.btnApples_Click);
            this.btnApples.MouseEnter += new System.EventHandler(this.changeColour);
            this.btnApples.MouseLeave += new System.EventHandler(this.colourChange);
            // 
            // btnOranges
            // 
            this.btnOranges.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOranges.Location = new System.Drawing.Point(130, 99);
            this.btnOranges.Name = "btnOranges";
            this.btnOranges.Size = new System.Drawing.Size(84, 29);
            this.btnOranges.TabIndex = 1;
            this.btnOranges.Text = "Oranges";
            this.btnOranges.UseVisualStyleBackColor = true;
            this.btnOranges.Click += new System.EventHandler(this.btnOranges_Click);
            this.btnOranges.MouseEnter += new System.EventHandler(this.changeColour);
            this.btnOranges.MouseLeave += new System.EventHandler(this.colourChange);
            // 
            // chkApples
            // 
            this.chkApples.AutoSize = true;
            this.chkApples.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkApples.Location = new System.Drawing.Point(23, 33);
            this.chkApples.Name = "chkApples";
            this.chkApples.Size = new System.Drawing.Size(79, 21);
            this.chkApples.TabIndex = 2;
            this.chkApples.Text = "Apples";
            this.chkApples.UseVisualStyleBackColor = true;
            this.chkApples.CheckedChanged += new System.EventHandler(this.chkApples_CheckedChanged);
            // 
            // txtTotApples
            // 
            this.txtTotApples.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotApples.Location = new System.Drawing.Point(246, 31);
            this.txtTotApples.Name = "txtTotApples";
            this.txtTotApples.Size = new System.Drawing.Size(100, 22);
            this.txtTotApples.TabIndex = 3;
            this.txtTotApples.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotApples.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumbersOnly);
            // 
            // chkOranges
            // 
            this.chkOranges.AutoSize = true;
            this.chkOranges.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOranges.Location = new System.Drawing.Point(23, 83);
            this.chkOranges.Name = "chkOranges";
            this.chkOranges.Size = new System.Drawing.Size(92, 21);
            this.chkOranges.TabIndex = 2;
            this.chkOranges.Text = "Oranges";
            this.chkOranges.UseVisualStyleBackColor = true;
            this.chkOranges.CheckedChanged += new System.EventHandler(this.chkOranges_CheckedChanged);
            // 
            // txtTotOranges
            // 
            this.txtTotOranges.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotOranges.Location = new System.Drawing.Point(246, 81);
            this.txtTotOranges.Name = "txtTotOranges";
            this.txtTotOranges.Size = new System.Drawing.Size(100, 22);
            this.txtTotOranges.TabIndex = 3;
            this.txtTotOranges.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotOranges.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumbersOnly);
            // 
            // lblTax
            // 
            this.lblTax.AutoSize = true;
            this.lblTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTax.Location = new System.Drawing.Point(21, 31);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(34, 17);
            this.lblTax.TabIndex = 1;
            this.lblTax.Text = "Tax";
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.AutoSize = true;
            this.lblSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubTotal.Location = new System.Drawing.Point(21, 70);
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Size = new System.Drawing.Size(73, 17);
            this.lblSubTotal.TabIndex = 1;
            this.lblSubTotal.Text = "SubTotal";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(21, 108);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(45, 17);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "Total";
            // 
            // txtTax
            // 
            this.txtTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTax.Location = new System.Drawing.Point(176, 31);
            this.txtTax.Name = "txtTax";
            this.txtTax.Size = new System.Drawing.Size(253, 22);
            this.txtTax.TabIndex = 1;
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.Location = new System.Drawing.Point(176, 67);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Size = new System.Drawing.Size(253, 22);
            this.txtSubTotal.TabIndex = 1;
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(176, 105);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(253, 22);
            this.txtTotal.TabIndex = 1;
            // 
            // btnTotal
            // 
            this.btnTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotal.Location = new System.Drawing.Point(14, 393);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(84, 29);
            this.btnTotal.TabIndex = 1;
            this.btnTotal.Text = "Total";
            this.btnTotal.UseVisualStyleBackColor = true;
            this.btnTotal.Click += new System.EventHandler(this.btnTotal_Click);
            this.btnTotal.MouseEnter += new System.EventHandler(this.changeColour);
            this.btnTotal.MouseLeave += new System.EventHandler(this.colourChange);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(185, 393);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(84, 29);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.MouseEnter += new System.EventHandler(this.changeColour);
            this.btnReset.MouseLeave += new System.EventHandler(this.colourChange);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(345, 393);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(84, 29);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseEnter += new System.EventHandler(this.changeColour);
            this.btnExit.MouseLeave += new System.EventHandler(this.colourChange);
            // 
            // multiTxtReceipt
            // 
            this.multiTxtReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiTxtReceipt.Location = new System.Drawing.Point(24, 150);
            this.multiTxtReceipt.Multiline = true;
            this.multiTxtReceipt.Name = "multiTxtReceipt";
            this.multiTxtReceipt.Size = new System.Drawing.Size(405, 237);
            this.multiTxtReceipt.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 683);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Checkout System";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblCostOfItem;
        private System.Windows.Forms.Label lblShoppingCart;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnOranges;
        private System.Windows.Forms.Button btnApples;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.TextBox txtTotOranges;
        private System.Windows.Forms.CheckBox chkOranges;
        private System.Windows.Forms.TextBox txtTotApples;
        private System.Windows.Forms.CheckBox chkApples;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnTotal;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.TextBox txtTax;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblSubTotal;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.TextBox multiTxtReceipt;
    }
}

